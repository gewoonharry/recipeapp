import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })

export class RecipeService {

    constructor(private http: HttpClient) {}

    getRecipes() {
        return this.http.get('/assets/recipesData.json');
    }

}
