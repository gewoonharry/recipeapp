import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecipeListContainerComponent } from './recipes/recipe-list/recipe-list.container';
import { RecipeAddContainerComponent } from './recipes/recipe-add/recipe-add.container';
import { RecipeEditContainerComponent } from './recipes/recipe-edit/recipe-edit.container';

const routes: Routes = [
  { path: '', redirectTo: '/recipes', pathMatch: 'full' },
  { path: 'recipes', component: RecipeListContainerComponent },
  { path: 'add', component: RecipeAddContainerComponent },
  { path: 'edit', component: RecipeEditContainerComponent },
  { path: '**', component: RecipeListContainerComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
