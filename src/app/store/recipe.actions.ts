import { Action } from '@ngrx/store';
import { Recipe } from '../recipe.model';

export const FETCH_RECIPES_START = '[FETCH-DATA] Load data start';
export const FETCH_RECIPES_SUCCES = '[FETCH-DATA] Load data succes';
export const FETCH_RECIPES_FAILED = '[FETCH-DATA] Load data failed';
export const SELECT_RECIPE = '[RECIPE-LIST] Recipe selected';
export const ADD_RECIPE = '[ADD-RECIPE] Recipe added';
export const REMOVE_INGREDIENT = '[EDIT-RECIPE] Ingredient removed';
export const REMOVE_INSTRUCTION = '[EDIT-RECIPE] Instruction removed';
export const REMOVE_RECIPE = '[EDIT-RECIPE] Recipe Removed';
export const ADD_INGREDIENT = '[EDIT-RECIPE] Ingredient added';
export const ADD_INSTRUCTION = '[EDIT-RECIPE] Instruction added';
export const EDIT_RECIPE = '[EDIT-RECIPE] Recipe edited';
export const EDIT_IMAGE = '[EDIT-RECIPE] Image edited';
export const REMOVE_IMAGE = '[EDIT-RECIPE] Image removed';

export class FetchRecipesStart implements Action {
  readonly type = FETCH_RECIPES_START;
}

export class FetchRecipesSucces implements Action {
  readonly type = FETCH_RECIPES_SUCCES;

  constructor(public payload: { data: any }) {}
}

export class FetchRecipesFailed implements Action {
  readonly type = FETCH_RECIPES_FAILED;

  constructor(public payload: { error: any }) {}
}


export class SelectRecipe implements Action {
  readonly type = SELECT_RECIPE;

  constructor(public payload: number) {}
}

export class AddRecipe implements Action {
  readonly type = ADD_RECIPE;

  constructor(public payload: Recipe) {}
}

export class RemoveIngredient implements Action {
  readonly type = REMOVE_INGREDIENT;

  constructor(public payload: {
    recipeId: number,
    itemId: number,
  }) {}
}

export class RemoveInstruction implements Action {
  readonly type = REMOVE_INSTRUCTION;

  constructor(public payload: {
    recipeId: number,
    itemId: number,
  }) {}
}

export class RemoveRecipe implements Action {
  readonly type = REMOVE_RECIPE;

  constructor(public payload: number) {}
}

export class AddIngredient implements Action {
  readonly type = ADD_INGREDIENT;

  constructor(public payload: {
    id: number,
    ingredient: {
      name: string,
      amount: string
    }
  }) {}
}

export class AddInstruction implements Action {
  readonly type = ADD_INSTRUCTION;

  constructor(public payload: {
    id: number,
    instruction: {
      step: number
      shortDesc: string,
      desc: string
    }
  }) {}
}

export class EditRecipe implements Action {
  readonly type = EDIT_RECIPE;

  constructor(public payload: {
    id: number,
    name: string
    desc: string,
    rating: number,
    friendly: number,
    difficulty: number,
    time: number,
    costs: number,
  }) {}
}

export class EditImage implements Action {
  readonly type = EDIT_IMAGE;

  constructor(public payload: {
    id: number,
    image: boolean,
    imageURL: string
  }) {}
}

export class RemoveImage implements Action {
  readonly type = REMOVE_IMAGE;

  constructor(public payload: number) {}
}

export type RecipeActions =
  | FetchRecipesStart
  | FetchRecipesSucces
  | FetchRecipesFailed
  | SelectRecipe
  | AddRecipe
  | RemoveIngredient
  | RemoveInstruction
  | RemoveRecipe
  | AddIngredient
  | AddInstruction
  | EditRecipe
  | EditImage
  | RemoveImage;

