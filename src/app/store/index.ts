import { createSelector, createFeatureSelector } from '@ngrx/store';
import { RecipeState } from './recipe.reducer';

export const getRecipeState = createFeatureSelector<RecipeState>('recipeStore');
export const getLoading = createSelector(getRecipeState, (state: RecipeState) => state.loading);
export const getDataLoaded = createSelector(getRecipeState, (state: RecipeState) => state.dataLoaded);
export const getRecipes = createSelector(getRecipeState, (state: RecipeState) => state.recipes);
export const getSelectedRecipe = createSelector(getRecipeState, (state: RecipeState) => state.selectedRecipe);

