import * as RecipeActions from './recipe.actions';
import { Recipe } from '../recipe.model';

export interface RecipeState {
  selectedRecipe: Recipe;
  recipes: Recipe[];
  loading: boolean;
  error: null;
  dataLoaded: boolean;
}

export const initialState: RecipeState = {
  selectedRecipe: {
    id: null,
    name: null,
    desc: null,
    rating: null,
    friendly: null,
    difficulty: null,
    time: null,
    costs: null,
    image: false,
    imageURL: null,
    ingredients: [{
      name: null,
      amount: null
    }],
    instructions: [{
      step: null,
      shortDesc: null,
      desc: null
    }]
  },
  recipes: [],
  loading: false,
  error: null,
  dataLoaded: false
};

export function RecipeReducer(
  state = initialState,
  action: RecipeActions.RecipeActions) {
  switch (action.type) {

    case RecipeActions.FETCH_RECIPES_START: {
      return {
        ...state,
        loading: true,
        error: null
      };
    }

    case RecipeActions.FETCH_RECIPES_SUCCES: {
      return {
        ...state,
        loading: false,
        recipes: action.payload.data,
        dataLoaded: true
      };
    }

    case RecipeActions.FETCH_RECIPES_FAILED: {
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };
    }

    case RecipeActions.SELECT_RECIPE:
      state.recipes.map(recipe => {
        if (recipe.id === action.payload) {
          state.selectedRecipe = recipe;
        }
      });
      return {
        ...state,
        selectedRecipe: state.selectedRecipe
      };

    case RecipeActions.ADD_RECIPE:
      return {
        ...state,
        recipes: [...state.recipes, action.payload]
      };

    case RecipeActions.REMOVE_INGREDIENT:
      state.recipes.map(recipe => {
        if (recipe.id === action.payload.recipeId) {
          recipe.ingredients.splice(action.payload.itemId, 1);
        }
      });
      return {
        ...state,
        recipes: [...state.recipes]
      };

    case RecipeActions.REMOVE_INSTRUCTION:
      state.recipes.map(recipe => {
        if (recipe.id === action.payload.recipeId) {
          recipe.instructions.splice(action.payload.itemId, 1);
        }
      });
      return {
        ...state,
        recipes: [...state.recipes]
      };

    case RecipeActions.REMOVE_RECIPE:
      const recipeFilter = state.recipes.filter(recipe => recipe.id !== action.payload);
      state.selectedRecipe = {
        id: null,
        name: null,
        desc: null,
        rating: null,
        friendly: null,
        difficulty: null,
        time: null,
        costs: null,
        image: false,
        imageURL: null,
        ingredients: [{
          name: null,
          amount: null
        }],
        instructions: [{
          step: null,
          shortDesc: null,
          desc: null
        }]
      };
      return {
        ...state,
        recipes: recipeFilter
      };

    case RecipeActions.ADD_INGREDIENT:
      state.recipes.map(recipe => {
        if (recipe.id === action.payload.id) {
          recipe.ingredients.push(action.payload.ingredient);
        }
      });
      return {
        ...state
      };

    case RecipeActions.ADD_INSTRUCTION:
      state.recipes.map(recipe => {
        if (recipe.id === action.payload.id) {
          recipe.instructions.push(action.payload.instruction);
        }
      });
      return {
        ...state
      };

    case RecipeActions.EDIT_RECIPE:
      state.recipes.map(recipe => {
        if (recipe.id === action.payload.id) {
          recipe.name = action.payload.name;
          recipe.desc = action.payload.desc;
          recipe.rating = action.payload.rating;
          recipe.friendly = action.payload.friendly;
          recipe.difficulty = action.payload.difficulty;
          recipe.time = action.payload.time;
          recipe.costs = action.payload.costs;
        }
      });
      return {
        ...state
      };

    case RecipeActions.EDIT_IMAGE:
      state.recipes.map(recipe => {
        if (recipe.id === action.payload.id) {
          recipe.image = action.payload.image;
          recipe.imageURL = action.payload.imageURL;
        }
      });
      return {
        ...state
      };

      case RecipeActions.REMOVE_IMAGE:
          state.recipes.map(recipe => {
            if (recipe.id === action.payload) {
              recipe.image = false;
              recipe.imageURL = null;
            }
          });
          return {
            ...state
          };

    default:
      return state;
  }
}
