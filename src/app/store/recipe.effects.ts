import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';
import { RecipeService } from '../recipe.service';
import * as RecipeActions from './recipe.actions';

@Injectable()

export class RecipeEffects {

    constructor(
        private actions: Actions,
        private recipeService: RecipeService
    ) {}

    @Effect()
    loadData = this.actions.pipe(
        ofType(RecipeActions.FETCH_RECIPES_START),
        switchMap(() => {
            return this.recipeService.getRecipes().pipe(
                map(data => new RecipeActions.FetchRecipesSucces({ data })),
                catchError(error =>
                    of (new RecipeActions.FetchRecipesFailed({ error }))
                )
            );
        })
    );
}
