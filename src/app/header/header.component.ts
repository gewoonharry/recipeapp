import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

  closeNav() {
    const uncheck = document.getElementById('nav-toggle') as HTMLInputElement;
    uncheck.checked = false;
  }

}
