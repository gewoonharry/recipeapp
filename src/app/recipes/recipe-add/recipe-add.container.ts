import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { getRecipes, getDataLoaded } from '../../store/index';
import { Observable, Subscription } from 'rxjs';
import { Recipe } from '../../recipe.model';
import * as RecipeActions from '../../store/recipe.actions';

@Component({
  selector: 'app-recipe-add-container',
  template: `
    <app-recipe-add
      (recipeAddEmitter)="recipeAddEmitter($event)"
    >
    </app-recipe-add>
  `
})
export class RecipeAddContainerComponent implements OnInit, OnDestroy {

  public recipes$: Observable<Recipe[]>;
  private recipes: Recipe[];
  private recipeSub: Subscription;
  private lastRecipeId: number;
  private dataLoaded$: Observable<boolean>;
  private dataLoadedSub: Subscription;
  private dataLoaded = false;
  private subscription: Subscription = new Subscription();
  private recipeAddEmitter(recipe: Recipe) {
    this.lastRecipeId++;
    recipe.id = this.lastRecipeId;
    if (!recipe.id) {
      recipe.id = 1;
    }
    this.store.dispatch(new RecipeActions.AddRecipe(recipe));
  }

  constructor(private store: Store<{ recipeStore }>) {}

  ngOnInit() {
    this.recipes$ = this.store.pipe(select(getRecipes));
    this.dataLoaded$ = this.store.pipe(select(getDataLoaded));
    this.recipeSub = this.recipes$.subscribe(recipes => {
        this.recipes = recipes;
    });
    this.dataLoadedSub = this.dataLoaded$.subscribe(dataLoaded => {
      this.dataLoaded = dataLoaded;
    });
    this.recipes.map(recipe => {
        this.lastRecipeId = recipe.id;
    });
    if (!this.dataLoaded) {
      this.store.dispatch(new RecipeActions.FetchRecipesStart());
    }
    this.subscription
      .add(this.dataLoadedSub)
      .add(this.recipeSub);
  }

  ngOnDestroy() {
      this.subscription.unsubscribe();
  }

}
