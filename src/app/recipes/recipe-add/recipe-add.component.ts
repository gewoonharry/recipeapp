import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Recipe } from '../../recipe.model';

@Component({
  selector: 'app-recipe-add',
  templateUrl: './recipe-add.component.html',
  styleUrls: ['./recipe-add.component.scss']
})
export class RecipeAddComponent implements OnInit {
  @Output() recipeAddEmitter = new EventEmitter<Recipe>();
  public ingredients = [];
  public instructions = [];
  private recipeAdd: any = {
    id: null,
    name: null,
    desc: null,
    image: false,
    imageURL: null,
    rating: null,
    friendly: null,
    difficulty: null,
    time: null,
    costs: null,
    ingredients: [this.ingredients],
    instructions: [this.instructions]
  };
  public ingredientAdded = false;
  public step = 1;
  public instructionAdded = false;
  public recipeAdded = false;
  public formAddChecked = true;
  public checked = false;

  onChecked(checked) {
    this.checked = checked;
  }

  addIngredient(form: NgForm) {
    const ingredient = {
      name: form.value.ingrName,
      amount: form.value.amount
    };
    this.ingredients.push(ingredient);
    this.ingredientAdded = true;
    form.reset();
  }

  addInstruction(form: NgForm) {
    const instruction = {
      step: this.step,
      shortDesc: form.value.shortDesc,
      desc: form.value.instruction
    };
    this.instructions.push(instruction);
    this.step++;
    this.instructionAdded = true;
    form.reset();
  }

  addRecipe(form: NgForm) {
    if (form.value.image) {
      this.recipeAdd.image = true;
      this.recipeAdd.imageURL = form.value.imageURL;
    }
    this.formAddChecked = true;
    this.recipeAdd.name = form.value.name;
    this.recipeAdd.desc = form.value.desc;
    this.recipeAdd.rating = form.value.rating;
    this.recipeAdd.friendly = form.value.friendly;
    this.recipeAdd.difficulty = form.value.difficulty;
    this.recipeAdd.time = form.value.time;
    this.recipeAdd.costs = form.value.costs;
    this.recipeAdd.ingredients = this.ingredients;
    this.recipeAdd.instructions = this.instructions;
    this.recipeAddEmitter.emit(this.recipeAdd);
    this.recipeAdd = {};
    this.ingredients = [];
    this.instructions = [];
    this.step = 1;
    this.recipeAdded = true;
    this.checked = false;
    form.reset();
  }

  addAnotherRecipe() {
    this.recipeAdded = false;
  }

  constructor() {
  }

  ngOnInit() {
  }

}
