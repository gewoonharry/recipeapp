import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { getSelectedRecipe, getLoading } from '../../store/index';
import { Observable } from 'rxjs';
import { Recipe } from '../../recipe.model';

@Component({
  selector: 'app-recipe-detail-container',
  template: `
    <app-recipe-detail
        [recipe]="selectedRecipe$ | async"
    >
    </app-recipe-detail>
  `
})
export class RecipeDetailContainerComponent implements OnInit {

  public selectedRecipe$: Observable<Recipe>;
  constructor(private store: Store<{ recipeStore }>) {
  }

  ngOnInit() {
    this.selectedRecipe$ = this.store.pipe(select(getSelectedRecipe));
  }

}
