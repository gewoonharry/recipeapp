import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Recipe } from '../../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.scss']
})

export class RecipeListComponent implements OnInit {

    @Input() recipes: Recipe[];
    @Input() loading: boolean;
    @Input() selectedRecipe: Recipe;
    @Output() selectedRecipeEmitter = new EventEmitter<number>();

  ngOnInit() {
  }

  onSelectRecipe(id: number) {
    this.selectedRecipeEmitter.emit(id);
  }
}
