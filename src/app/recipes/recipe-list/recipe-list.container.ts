import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { getRecipes, getLoading, getSelectedRecipe, getDataLoaded } from '../../store/index';
import { Observable, Subscription } from 'rxjs';
import { Recipe } from '../../recipe.model';
import * as RecipeActions from '../../store/recipe.actions';

@Component({
  selector: 'app-recipe-list-container',
  template: `
    <app-recipe-list
      [recipes]="recipes$ | async"
      [loading]="loading$ | async"
      [selectedRecipe]="selectedRecipe$ | async"
      (selectedRecipeEmitter)="selectedRecipe($event)"
      >
    </app-recipe-list>
  `
})
export class RecipeListContainerComponent implements OnInit, OnDestroy {
  public loading$: Observable<boolean>;
  public recipes$: Observable<Recipe[]>;
  public selectedRecipe$: Observable<Recipe>;
  private dataLoaded$: Observable<boolean>;
  private subscription: Subscription = new Subscription();
  private dataloaded = false;
  private selectedRecipe(id: number) {
      this.store.dispatch(new RecipeActions.SelectRecipe(id));
  }

  constructor(
    private store: Store<{ recipeStore }>,
  ) {
  }

  ngOnInit() {
    this.dataLoaded$ = this.store.pipe(select(getDataLoaded));
    this.subscription = this.dataLoaded$.subscribe(dataLoaded => {
        this.dataloaded = dataLoaded;
    });
    if (!this.dataloaded) {
        this.store.dispatch(new RecipeActions.FetchRecipesStart());
    }
    this.recipes$ = this.store.pipe(select(getRecipes));
    this.loading$ = this.store.pipe(select(getLoading));
    this.selectedRecipe$ = this.store.pipe(select(getSelectedRecipe));
  }

  ngOnDestroy() {
      this.subscription.unsubscribe();
  }

}
