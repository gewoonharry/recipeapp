import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Recipe } from '../../recipe.model';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.scss']
})
export class RecipeEditComponent implements OnInit, OnDestroy {
  @Input() recipes: Recipe[];
  @Input() loading: boolean;
  @Input() selectedRecipe: Recipe;
  @Output() selectedRecipeEmitter = new EventEmitter<number>();
  @Output() editRecipeEmitter = new EventEmitter<Recipe>();
  @Output() editRecipeImageEmitter = new EventEmitter<{}>();
  @Output() removeImageEmitter = new EventEmitter<number>();
  @Output() removeRecipeEmitter = new EventEmitter<number>();
  @Output() removeIngredientEmitter = new EventEmitter<{}>();
  @Output() removeInstructionEmitter = new EventEmitter<{}>();
  @Output() addIngredientEmitter = new EventEmitter<{}>();
  @Output() addInstructionEmitter = new EventEmitter<{}>();

  private recipeEdit: any = {
    id: null,
    name: null,
    desc: null,
    image: false,
    imageURL: null,
    rating: null,
    friendly: null,
    difficulty: null,
    time: null,
    costs: null
  };
  public isSelected = false;
  public removeRecipeScreen: boolean;
  public confirm: boolean;
  public amount: number = null;
  public ingrName: string = null;
  public step: number = null;
  public shortDesc: string = null;
  public instrDesc: string = null;
  public setEditDetails = false;
  public setEditImage = false;
  public setEditIngredients = false;
  public setEditInstructions = false;
  public setEdited = false;

  onSetEdited(setEdited) {
    this.setEdited = setEdited;
  }

  onSelectRecipe(id: number) {
    this.selectedRecipeEmitter.emit(id);
    this.isSelected = true;
    this.setEdited = false;
    this.setEditDetails = false;
    this.setEditIngredients = false;
    this.setEditInstructions = false;
  }

  onEditDetails() {
    this.setEditDetails = true;
    this.setEditImage = false;
    this.setEditIngredients = false;
    this.setEditInstructions = false;
  }

  onEditImage() {
    this.setEditDetails = false;
    this.setEditImage = true;
    this.setEditIngredients = false;
    this.setEditInstructions = false;
  }

  onEditIngredients() {
    this.setEditDetails = false;
    this.setEditImage = false;
    this.setEditIngredients = true;
    this.setEditInstructions = false;
  }

  onEditInstructions() {
    this.setEditDetails = false;
    this.setEditImage = false;
    this.setEditIngredients = false;
    this.setEditInstructions = true;
  }

  editDetails(form: NgForm) {
    this.setEdited = true;
    this.recipeEdit.id = this.selectedRecipe.id;
    this.recipeEdit.name = form.value.name;
    this.recipeEdit.desc = form.value.desc;
    this.recipeEdit.rating = form.value.rating;
    this.recipeEdit.friendly = form.value.friendly;
    this.recipeEdit.difficulty = form.value.difficulty;
    this.recipeEdit.time = form.value.time;
    this.recipeEdit.costs = form.value.costs;
    if (form.value.name === '') {
      this.recipeEdit.name = this.selectedRecipe.name;
    }
    if (form.value.desc === '') {
      this.recipeEdit.desc = this.selectedRecipe.desc;
    }
    if (form.value.rating === '') {
      this.recipeEdit.rating = this.selectedRecipe.rating;
    }
    if (form.value.friendly === '') {
      this.recipeEdit.friendly = this.selectedRecipe.friendly;
    }
    if (form.value.difficulty === '') {
      this.recipeEdit.difficulty = this.selectedRecipe.difficulty;
    }
    if (form.value.time === '') {
      this.recipeEdit.time = this.selectedRecipe.time;
    }
    if (form.value.costs === '') {
      this.recipeEdit.costs = this.selectedRecipe.costs;
    }
    this.editRecipeEmitter.emit(this.recipeEdit);
  }

  editImage(form: NgForm) {
    const recipeImage = {
      id: this.selectedRecipe.id,
      image: true,
      imageURL: form.value.imageURL
    };
    this.editRecipeImageEmitter.emit(recipeImage);
  }

  removeImage() {
    this.removeImageEmitter.emit(this.selectedRecipe.id);
  }

  removeIngredient(itemId: number) {
    this.removeIngredientEmitter.emit({
      recipeId: this.selectedRecipe.id,
      itemId
    });
    }

  removeInstruction(itemId: number) {
    this.removeInstructionEmitter.emit({
      recipeId: this.selectedRecipe.id,
      itemId
    });
  }

  addIngredient(form: NgForm) {
    const ingredient = {
      name: form.value.ingrName,
      amount: form.value.amount
    };
    this.addIngredientEmitter.emit({ id: this.selectedRecipe.id, ingredient });
    form.reset();
  }

  addInstruction(form: NgForm) {
    this.step = this.selectedRecipe.instructions.length + 1;
    const instruction = {
      step: this.step,
      shortDesc: form.value.shortDesc,
      desc: form.value.instruction
    };
    this.addInstructionEmitter.emit({ id: this.selectedRecipe.id, instruction });
    this.step++;
    form.reset();
  }

  removeRecipe() {
    this.removeRecipeScreen = true;
  }

  removeRecipeConfirm(confirm: boolean) {
    if (confirm) {
      this.removeRecipeEmitter.emit(this.selectedRecipe.id);
    }
    this.removeRecipeScreen = false;
    this.confirm = false;
  }

  constructor() {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
  }
}

