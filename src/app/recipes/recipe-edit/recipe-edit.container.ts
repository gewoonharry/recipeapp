import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { getRecipes, getLoading, getSelectedRecipe, getDataLoaded } from '../../store/index';
import { Observable, Subscription } from 'rxjs';
import { Recipe } from '../../recipe.model';
import * as RecipeActions from '../../store/recipe.actions';

@Component({
  selector: 'app-recipe-edit-container',
  template: `
    <app-recipe-edit
        [recipes]="recipes$ | async"
        [loading]="loading$ | async"
        [selectedRecipe]="selectedRecipe$ | async"
        (selectedRecipeEmitter)="selectedRecipe($event)"
        (editRecipeEmitter)="editRecipe($event)"
        (editRecipeImageEmitter)="editImage($event)"
        (removeImageEmitter)="removeImage($event)"
        (removeRecipeEmitter)="removeRecipe($event)"
        (removeIngredientEmitter)="removeIngredient($event)"
        (removeInstructionEmitter)="removeInstruction($event)"
        (addIngredientEmitter)="addIngredient($event)"
        (addInstructionEmitter)="addInstruction($event)"
    >
    </app-recipe-edit>
  `
})
export class RecipeEditContainerComponent implements OnInit, OnDestroy {

    public loading$: Observable<boolean>;
    public recipes$: Observable<Recipe[]>;
    public selectedRecipe$: Observable<Recipe>;
    private dataLoaded$: Observable<boolean>;
    private subscription: Subscription = new Subscription();
    private dataloaded = false;
    private selectedRecipe(id: number) {
        this.store.dispatch(new RecipeActions.SelectRecipe(id));
    }
    private editRecipe(recipe: Recipe) {
        this.store.dispatch(new RecipeActions.EditRecipe(recipe));
    }
    private editImage(recipeImage) {
        this.store.dispatch(new RecipeActions.EditImage(recipeImage));
    }
    private removeImage(recipeId) {
        this.store.dispatch(new RecipeActions.RemoveImage(recipeId));
    }
    private removeRecipe(recipeId) {
        this.store.dispatch(new RecipeActions.RemoveRecipe(recipeId));
    }
    private removeIngredient(ingredient) {
        this.store.dispatch(new RecipeActions.RemoveIngredient(ingredient));
    }
    private removeInstruction(instruction) {
        this.store.dispatch(new RecipeActions.RemoveInstruction(instruction));
    }
    private addIngredient(ingredient) {
        this.store.dispatch(new RecipeActions.AddIngredient(ingredient));
    }
    private addInstruction(instruction) {
        this.store.dispatch(new RecipeActions.AddInstruction(instruction));
    }

    constructor(private store: Store<{ recipeStore }>) {

    }

    ngOnInit() {
        this.dataLoaded$ = this.store.pipe(select(getDataLoaded));
        this.subscription = this.dataLoaded$.subscribe(dataLoaded => {
            this.dataloaded = dataLoaded;
        });
        if (!this.dataloaded) {
            this.store.dispatch(new RecipeActions.FetchRecipesStart());
        }
        this.recipes$ = this.store.pipe(select(getRecipes));
        this.loading$ = this.store.pipe(select(getLoading));
        this.selectedRecipe$ = this.store.pipe(select(getSelectedRecipe));
      }

      ngOnDestroy() {
          this.subscription.unsubscribe();
      }

}
