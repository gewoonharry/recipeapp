import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { RecipeReducer } from './store/recipe.reducer';
import { EffectsModule } from '@ngrx/effects';
import { RecipeEffects } from './store/recipe.effects';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { RecipeListContainerComponent } from './recipes/recipe-list/recipe-list.container';
import { RecipeListComponent } from './recipes/recipe-list/recipe-list.component';
import { RecipeDetailContainerComponent } from './recipes/recipe-detail/recipe-detail.container';
import { RecipeDetailComponent } from './recipes/recipe-detail/recipe-detail.component';
import { RecipeAddContainerComponent } from './recipes/recipe-add/recipe-add.container';
import { RecipeAddComponent } from './recipes/recipe-add/recipe-add.component';
import { RecipeEditContainerComponent } from './recipes/recipe-edit/recipe-edit.container';
import { RecipeEditComponent } from './recipes/recipe-edit/recipe-edit.component';
import { AppEffects } from './app.effects';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RecipeListContainerComponent,
    RecipeListComponent,
    RecipeDetailContainerComponent,
    RecipeDetailComponent,
    RecipeAddContainerComponent,
    RecipeAddComponent,
    RecipeEditContainerComponent,
    RecipeEditComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    StoreModule.forRoot({recipeStore: RecipeReducer}),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production,
    }),
    EffectsModule.forRoot([RecipeEffects, AppEffects]),
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
