export class Recipe {
    id: number;
    name: string;
    desc: string;
    image: boolean;
    imageURL: string;
    rating: number;
    friendly: number;
    difficulty: number;
    time: number;
    costs: number;
    ingredients: [{
        name: string;
        amount: string;
    }];
    instructions: [{
        step: number;
        shortDesc: string;
        desc: string;
    }];
}
